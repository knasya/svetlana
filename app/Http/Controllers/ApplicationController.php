<?php

namespace App\Http\Controllers;
use App\Models\Application;
use Illuminate\Http\Request;
use Lunaweb\RecaptchaV3\Facades\RecaptchaV3;
use Illuminate\Support\Facades\Validator;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function application(Request $request){

        $validator = Validator::make($request->all(), [
            'g-recaptcha-response' => 'required'
        ]);
    
        if ($validator->fails()) {
            return redirect('/order')->with('info', 'Не указана reCAPTCHA');
        }

        if (!RecaptchaV3::verify($request->request->get("g-recaptcha-response"))) {
            return redirect('/order')->with('info', 'Неверная reCAPTCHA');
        }
    
        Application::create([
            'name'=>$request->name,
            'phone'=>$request->phone,
            'application'=>$request->application,
        ]);
    
        return redirect('/order')->with('info', 'Заявка отправлена, в скором времени с вами свяжутся');
    }

    public function index()
    {
        return view('admin.applications.index', ['applications'=>Application::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        $application->delete();
        return redirect('admin/applications')->with('info', 'Заявка удалена');
    }
}
