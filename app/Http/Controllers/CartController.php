<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Order;
use App\Models\Product;

class CartController extends Controller
{
    public function add($product_id)
    {
        $order = Auth::user()->orders()->firstWhere('status', 'в корзине');

        if (is_null($order)) {
            $order = new Order;
            $order->user_id = Auth::user()->id;
            $order->status = 'в корзине';
            $order->save();
        }

        $product = $order->products()->find($product_id);

        if (!$product){
            $order->products()->attach($product_id, ['qty'=>1]);
            return redirect()->back()->with('info', 'Товар добавлен в корзину');
        }

        return redirect()->back()->with('info', 'Товар уже есть в корзине');
    }

    public function show()
    {
        $order = Auth::user()->orders()->firstWhere('status', 'в корзине');

        if (is_null($order)) {
            $order = new Order;
            $order->user_id = Auth::user()->id;
            $order->status = 'в корзине';
            $order->save();
        }

        return view('cart', ['products'=>$order->products, 'user'=>Auth::user()]);
    }


    public function change($product_id, Request $request)
    {
        $order = Auth::user()->orders()->firstWhere('status', 'в корзине');

        $order->products()->updateExistingPivot($product_id, ['qty'=>$request->qty]);

        return redirect()->back()->with('info', 'Количество изменено');
    }
    
    public function delete($product_id)
    {
        $order = Auth::user()->orders()->firstWhere('status', 'в корзине');

        $order->products()->detach($product_id);

        return redirect()->back()->with('info', 'Товар удален');
    }
}
