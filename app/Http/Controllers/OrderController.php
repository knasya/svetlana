<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::where('status', '!=', 'в корзине')
                            ->withCount('products')
                            ->get();
        return view('admin.orders.index', ['orders'=>$orders]);
    }

    public function confirm(Order $order)
    {
        $order->status = 'подтвержден';
        $order->save();
        return redirect('admin/orders')->with('info', 'Статус заказа изменен');
    }

    public function cancel(Request $request, Order $order)
    {
        $order->status = 'отменен';
        $order->comment = $request->comment;
        $order->save();
        return redirect('admin/orders')->with('info', 'Статус заказа изменен');
    }

    public function send(Request $request)
    {
        $order = Auth::user()->orders()->firstWhere('status', 'в корзине');
        $order->status = 'новый';
        $order->save();

        $order = new Order;
        $order->user_id = Auth::user()->id;
        $order->status = 'в корзине';
        $order->save();

        $address = $request->request;
        $user = Auth::user();
        $user->address = $address->get("address");
        $user->entrance = $address->get("entrance");
        $user->apartment = $address->get("apartment");
        $user->floor = $address->get("floor");
        $user->intercom = $address->get("intercom");
        $user->save();

        return redirect('orders/list')->with('info', 'Заказ отправлен');
    }

    public function list()
    {
        $orders = Auth::user()->orders()->orderBy('created_at')->get();

        return view('orders', ['orders'=>$orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->products()->detach();
        $order->delete();
        return redirect()->back()->with('info', 'заказ удален');
    }
}
