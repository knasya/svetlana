<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use GuzzleHttp\Promise\Create;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('name')->get();
        return view('admin.products.index', ['products'=>$products, 'categories'=>Category::all()]);
    }

    public function filter(Request $request)
    {
        if ($request->filter == 0){
            $products = Product::orderBy($request->sort)
                                ->get();
        } else {
            $products = Category::find($request->filter)
                                ->products()
                                ->orderBy($request->sort)
                                ->get();
        }
        return view('admin.products.index', ['products'=>$products, 'categories'=>Category::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create', ['categories'=>Category::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('img')){
            $img = $request->file('img')->store('img_store');
        }

        Product::create(['category_id'=>$request->category_id,
        'name'=>$request->name,
        'price'=>$request->price,
        'size'=>$request->size,
        'description'=>$request->description,
        'compound'=>$request->compound,
        'qty'=>$request->qty,
        'img'=>$img ?? NULL,
        ]);

        return redirect('admin/products')->with('info', 'Товар добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin.products.show', ['product'=>$product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin.products.edit', ['product'=>$product, 'categories'=>Category::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->category_id = $request->category_id;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->size = $request->size;
        $product->description = $request->description;
        $product->compound = $request->compound;
        $product->qty = $request->qty;
        if($request->hasFile('img')){
            $img = $request->file('img')->store('img');
        }
        $product->save();

        return redirect('admin/products')->with('info', 'Товар обновлен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('admin/products')->with('info', 'Товар удален');
    }
}
