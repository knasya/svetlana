<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Order;
use Lunaweb\RecaptchaV3\Facades\RecaptchaV3;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function create(){
        return view('auth.create');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'g-recaptcha-response' => 'required'
        ]);
    
        if ($validator->fails()) {
            return redirect('/')->with('info', 'Не указана reCAPTCHA');
        }

        if (!RecaptchaV3::verify($request->request->get("g-recaptcha-response"))) {
            return redirect('/')->with('info', 'Неверная reCAPTCHA');
        }

        $request->validate([
            'name' => ['regex:/^[А-Яа-я\- ]{1,}$/u', 'required'],
            'surname' => ['regex:/^[А-Яа-я\- ]{1,}$/u', 'required'],
            'patronymic' => ['regex:/^[А-Яа-я\- ]{0,}$/u', 'nullable'],
            'login' => ['regex:/^[0-9A-Za-z\-]+$/', 'unique:users', 'required'],
            'email'=>['email', 'unique:users', 'required'],
            'password'=>['confirmed','min:6', 'required'],
        ]);

        $user = User::create([
            'name'=>$request->name,
            'surname'=>$request->surname,
            'patronymic'=>$request->patronymic,
            'login'=>$request->login,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'address'=>$request->address,
            'entrance'=>$request->entrance,
            'apartment'=>$request->apartment,
            'floor'=>$request->floor,
            'intercom'=>$request->intercom,
        ]);

        Auth::login($user);
        $request->session()->regenerate();

        //Создание нового заказа (корзины) для нового пользователя
        $order = new Order;
        $order->user_id = Auth::user()->id;
        $order->status = 'в корзине';
        $order->save();

        return redirect('/')->with('info', 'Вы успешно зарегистрировались');

    }

    public function logout(){
        Auth::logout();
        return redirect('/')->with('info', 'Выход выполнен!');
    }

    public function loginform(){
        return view('auth.loginform');
    }

    public function login(Request $request){

        $credentials = $request->validate([
            'login' => ['required'],
            'password' => ['required'],
        ]);
 
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            if (Auth::user()->isAdmin){
                return redirect('/admin')->with('info', 'Вы зашли как администратор');
            } else {
                return redirect('/')->with('info', 'Вход выполнен!');
            }
        }
        return back()->withErrors(['Данные не соответствуют!']);
    }


    public function update(Request $request)
    {
        $user = User::firstWhere('id', Auth::user()->id);
        $user->address = $request->address;
        $user->entrance = $request->entrance;
        $user->apartment = $request->apartment;
        $user->floor = $request->floor;
        $user->intercom = $request->intercom;
        $user->save();
    }
}
