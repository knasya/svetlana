<?php

namespace App\Http\Controllers;
use App\Models\Review;
use Illuminate\Http\Request;
use Lunaweb\RecaptchaV3\Facades\RecaptchaV3;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function reviewsMore(Review $review){

        $reviews = Review::orderBy('updated_at')->where('publish', '=', '1')->get();
        return view(['reviewsMore', 'reviews'=>$reviews]);

    }

     public function review(Request $request){

        $validator = Validator::make($request->all(), [
            'g-recaptcha-response' => 'required'
        ]);
    
        if ($validator->fails()) {
            return redirect('/aboutBrand')->with('info', 'Не указана reCAPTCHA');
        }

        if (!RecaptchaV3::verify($request->request->get("g-recaptcha-response"))) {
            return redirect('/aboutBrand')->with('info', 'Неверная reCAPTCHA');
        }

        Review::create([
            'name'=>$request->name,
            'surname'=>$request->surname,
            'review'=>$request->review,
            'publish'=>false,
        ]);

        return redirect('/aboutBrand')->with('info', 'Отзыв отправлен, в скором времени его опубликуют');

    }

    public function index()
    {
        return view('admin.reviews.index', ['reviews'=>Review::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        return view('admin.reviews.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        $review->publish = !$request->publish;
        $review->save();
        return redirect('admin/reviews')->with('info', 'Отзыв опубликован');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        $review->delete();
        return redirect('admin/reviews')->with('info', 'Отзыв удален');
    }
}
