<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['title' => 'Платья'],
            ['title' => 'Юбки'],
            ['title' => 'Штаны'],
            ['title' => 'Футболки'],
            ['title' => 'Костюмы'],
            ['title' => 'Куртки'],
        ]);
    }
}
