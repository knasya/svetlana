<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
             ['name'=> 'Розовый костюм', 'img'=> '/img/2.png', 'price'=>9500, 'size'=> 'S',
             'description'=>'Отличный костюм', 'compound'=>'Хлопок', 'qty'=>1, 'category_id'=>5,
             'created_at'=>Now(), 'updated_at'=>Now()],
             ['name'=> 'Пальто', 'img'=> '/img/3.png', 'price'=>30000, 'size'=> 'S-М',
             'description'=>'Отличное пальто', 'compound'=>'Хлопок', 'qty'=>1, 'category_id'=>6,
             'created_at'=>Now(), 'updated_at'=>Now()],
             ['name'=> 'Синее платье', 'img'=> '/img/5.png', 'price'=>14000, 'size'=> 'S',
             'description'=>'Красивое платье', 'compound'=>'Атлас', 'qty'=>1, 'category_id'=>1,
             'created_at'=>Now(), 'updated_at'=>Now()],
             ['name'=> 'Фиолетовая кофта', 'img'=> '/img/23.png', 'price'=>5500, 'size'=> 'M',
             'description'=>'Хорошая кофта', 'compound'=>'Хлопок', 'qty'=>1, 'category_id'=>4,
             'created_at'=>Now(), 'updated_at'=>Now()],
        ]);
    }
}
