<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Анастасия',
                'surname' => 'Корсакова',
                'patronymic' => 'Николаевна',
                'login' => 'Admin',
                'email' => 'korsakova@gmail.com',
                'password' => '$2y$10$J0JMWQTX3vb9lK73EXajTek3qwn/sfETIM6WxaKx0Zr1HPVWUGy/O',
                'isAdmin' => 1,
                'address' => 'Садовая 10',
                'entrance' => '3',
                'apartment' => '36',
                'floor' => '1',
                'intercom' => '36',
                'created_at' => Now(),
                'updated_at' => Now(),
            ],
            [
                'name' => 'Алиса',
                'surname' => 'Селезнёва',
                'patronymic' => 'Николаевна',
                'login' => 'Alisa',
                'email' => 'alisa@gmail.com',
                'password' => '$2y$10$J0JMWQTX3vb9lK73EXajTek3qwn/sfETIM6WxaKx0Zr1HPVWUGy/O',
                'isAdmin' => 0,
                'address' => 'Ленинская 8',
                'entrance' => '1',
                'apartment' => '10',
                'floor' => '5',
                'intercom' => '10',
                'created_at' => Now(),
                'updated_at' => Now(),
            ],
            [
                'name' => 'Светлана',
                'surname' => 'Соколова',
                'patronymic' => 'Александровна',
                'login' => 'Sveta',
                'email' => 'sveta@gmail.com',
                'password' => '$2y$10$J0JMWQTX3vb9lK73EXajTek3qwn/sfETIM6WxaKx0Zr1HPVWUGy/O',
                'isAdmin' => 0,
                'address' => 'Липовая аллея 1',
                'entrance' => '1',
                'apartment' => '5',
                'floor' => '2',
                'intercom' => '5',
                'created_at' => Now(),
                'updated_at' => Now(),
            ],
        ]);
    }
}
