@extends('layout')

@section('content')

      <div class="container py-3 element-animation mb-5">
        <h2 class="fw-light mb-5 text-center">О магазине</h2>
      <div class="row mb-5">
        <div class="col-lg-6 ext-center">
            <img class="mx-auto d-block img-border" src="img/svetlana.jpg" alt="banner" height="400" width="430">
        </div>
        <div class="col-lg-6 text-center mb-3 d-flex align-items-center">
            <div class="col">
            <h4 class="fw-light mb-3">Одежда премиум класса достойная Вас! От дизайнера <br /> Светланы Золотовой. Комфорт и качество, <br /> быстрая доставка и примерка. Возможность заказать модель одежды с учетом ваших мерок. Помощь стилиста. Производство находится в Москве.</h4>
            </div>
        </div>
      </div>
      </div>

      <!-- Отзывы -->
      <h2 class="fw-light mb-5 text-center element-animation">Отзывы</h2>

      <div class="cards-holder row">
            <div class="card col-md-12">
              <div class="text-center">
                <div class="card-body text-center">
                  <h5 class="card-title">Ирина смирнова</h5>
                  <p class="card-text">Мне платье очень нравится. Ткань плотная, приятная к телу. Добротная. Смотрится платье дорого. И сидит по фигуре хорошо. Воротник сначала смущал, но он действительно подходит к платью.</p>
                </div>
              </div>
            </div>
            <div class="card col-md-12">
              <div class="text-center">
                <div class="card-body text-center">
                  <h5 class="card-title">Ксения Иванова</h5>
                  <p class="card-text">Потрясающе! Какую же красоту вы создаёте. Лучше одежды для женщины и быть не может!</p>
                </div>
              </div>
            </div>
            <div class="card col-md-12">
              <div class="text-center">
                <div class="card-body text-center">
                  <h5 class="card-title">Ирина Соколова</h5>
                  <p class="card-text">Добрый день! Получили с сестрой свою посылку с платьями! Спасибо Вам за красоту. Приятно порадовали цены и качество!</p>
                </div>
              </div>
            </div>
          </div>
          <div class="text-center mb-5 mt-5">
        <a class="btn btn-burgundy" href="{{route('reviewsMore')}}" role="button">Больше отзывов</a>
      </div>


      {{-- <div class="row element-animation">
        <div class="cards-holder">
          <div class="card" style="margin-top: 0;">
            <div class="text-center">
              <div class="card-body text-center">
                <h5 class="card-title">Екатерина Смирнова</h5>
                <p class="card-text">Мне платье очень нравится. Ткань плотная, приятная к телу. Добротная. Смотрится платье дорого. И сидит по фигуре хорошо. Воротник сначала смущал, но он действительно подходит к платью. </p>
              </div>
            </div>
          </div>
          <div class="card" style="margin-top: 0;">
            <div class="text-center">
              <div class="card-body text-center">
                <h5 class="card-title">Ксения Иванова</h5>
                <p class="card-text">Потрясающе! Какую же красоту вы создаёте. Лучше одежды для женщины и быть не может!</p>
              </div>
            </div>
          </div>
          <div class="card" style="margin-top: 0;">
            <div class="text-center">
              <div class="card-body text-center">
                <h5 class="card-title">Ирина Скоколова</h5>
                <p class="card-text">Добрый день! Получили с сестрой свою посылку с платьями! Спасибо Вам за красоту. Приятно порадовали цены и качество!</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="text-center mb-5 mt-5">
        <a class="btn btn-burgundy" href="{{route('reviewsMore')}}" role="button">Больше отзывов</a>
      </div> --}}

      <!-- Форма для отзывов -->
      <div class="container py-3 text-center">
      <h2 class="fw-light mb-5 text-center element-animation">Оставить отзыв</h2>
        <form method="post" action="{{route('aboutBrand')}}">
          @csrf
          <div class="row">
          <div class="col-md-6">
            <label for="inputName" class="form-label" for="name">Имя</label>
            <input type="text" class="form-control" id="inputName" name="name">
          </div>
          <div class="col-md-6">
            <label for="inputSurname" class="form-label" for="surname">Фамилия</label>
            <input type="text" name="surname" class="form-control" id="inputSurname">
          </div>
            <label for="exampleFormControlTextarea1" class="form-label" for="review">Оставить отзыв:</label>
            <textarea class="form-control mb-3" id="exampleFormControlTextarea1" rows="3" name="review"></textarea>
            <div class="g-recaptcha mb-3" data-sitekey="6LcQhikmAAAAAKUZayK4h4_aiEwLDUx7_os_xtU3"></div>
            {!! RecaptchaV3::initJs() !!}
            {!! RecaptchaV3::field("validate_captcha") !!}
            <input type="hidden" name="action" value="validate_captcha">
            <div class="col-12 text-center mb-3">
                <button type="submit" class="btn btn-burgundy">Отправить</button>
            </div>
          </div>
        </form>

      </div>
<? 

//    $error = true;
//    $secret = '6LcQhikmAAAAAOnHpm8EIXkkXApMrwmS3S45XdDu';
//    
//    if (!empty($_POST['g-recaptcha-response'])) {
//        $curl = curl_init('https://www.google.com/recaptcha/api/siteverify');
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_POST, true);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, 'secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
//        $out = curl_exec($curl);
//        curl_close($curl);
//        
//        $out = json_decode($out);
//        if ($out->success == true) {
//            $error = false;
//        } 
//    }
//        
//    if ($error) {
//        echo 'Ошибка заполнения капчи.';
//    }

?>
@endsection