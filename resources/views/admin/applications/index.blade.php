@extends('admin.layout')

@section('content')

    <!-- Каталог -->
    <div class="container py-3 element-animation vh-100">
        <table class="table table-striped">
  <thead>
    <tr>
        <th scope="col">Имя</th>
        <th scope="col">Телефон</th>
        <th scope="col">Комментарий</th>
        <th scope="col">Удалить</th>
    </tr>
  </thead>
  <tbody>
    
    @foreach ($applications as $application)
    <tr>
      <th scope="row">{{$application->name}}</th>
      <td>{{$application->phone}}</td>
      <td>{{$application->application}}</td>
      <td>
        <form method="POST" action="{{route('applications.destroy', $application)}}">
            @csrf
            @method('DELETE')
            <button class="btn btn-burgundy" type="submit">Удалить</button>
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
    
</table>
    </div>

@endsection