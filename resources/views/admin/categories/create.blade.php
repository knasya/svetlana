@extends('admin.layout')

@section('content')


<div class="container py-3 vh-100 element-animation">
        <h3 class="fw-light mb-5 text-center">Добавление новой категори товаров</h3>
        <form class="row g-3" method="post" action="{{route('categories.store')}}">
        @csrf
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputTitle" name="title" aria-describedy="title" placeholder="Название новой категории">
          </div>
          <div class="col-12 text-center mt-5">
            <button type="submit" class="btn btn-burgundy">Создать</button>
          </div>
        </form>
      </div>

@endsection