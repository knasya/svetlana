@extends('admin.layout')

@section('content')


<div class="container py-3 vh-100 element-animation">
        <h3 class="fw-light mb-5 text-center">Изменение категории {{$category->title}}</h3>
        <form class="row g-3" method="post" action="{{route('categories.update', $category)}}">
        @csrf
        @method('PUT')
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputTitle" name="title" value="{{$category->title}}" placeholder="Новое название категории">
          </div>
          <div class="col-12 text-center mt-5">
            <button type="submit" class="btn btn-burgundy">Изменить</button>
          </div>
        </form>
      </div>

@endsection