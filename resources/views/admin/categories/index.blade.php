@extends('admin.layout')

@section('content')


<table class="table table-striped">
  <thead>
    <tr>
        <th scope="col">Номер</th>
        <th scope="col">Название</th>
        <th scope="col">Изменить</th>
        <th scope="col">Удалить</th>
    </tr>
  </thead>
  <tbody>
    
    @foreach ($categories as $category)
    <tr>
      <th scope="row">{{$category->id}}</th>
      <td>{{$category->title}}</td>
      <td>
        <a class="btn btn-burgundy" href="{{route('categories.edit', $category)}}">Изменить</a>
      </td>
      <td>
        <form method="POST" action="{{route('categories.destroy', $category)}}">
            @csrf
            @method('DELETE')
            <button class="btn btn-burgundy" type="submit">Удалить</button>
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
    
</table>

<a href="{{route('categories.create')}}" class="btn btn-burgundy" role="button">
    Добавть новую категорию
</a>

@endsection