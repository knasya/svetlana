<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/logo.png" type="image/png">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <title>Студия Светланы Золотовой</title>
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-white element-animation">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Панель администратора</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link" href="{{route('categories.index')}}">Категории товаров</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('products.index')}}">Товары</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('admin.orders.index')}}">Заказы</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('applications.index')}}">Заявки</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('reviews.index')}}">Отзывы</a>
              </li>
            </ul>
            <form class="d-flex align-items-center">
                <div class="text-reset me-3">
                @auth
                    <a class="btn btn-burgundy" href="{{route('auth.logout')}}">Выход</a>
                @endauth
                </div>
            </form>
          </div>
        </div>
      </nav>
      
      <div class="container">
        @if (session('info'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session('info')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Закрыть">
                </button>
            </div>
        @endif
        @yield('content')
      </div>


    <script src="/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>