@extends('admin.layout')

@section('content')


<table class="table table-striped vh-100">
  <thead>
    <tr>
        <th scope="col">Дата</th>
        <th scope="col">ФИО, адрес</th>
        <th scope="col">Состав закза</th>
        <th scope="col">Статус</th>
        <th scope="col">Отмена</th>
    </tr>
  </thead>
  <tbody>
    
    @foreach ($orders as $order)
    <tr>
      <td scope="row">{{$order->created_at}}</td>
      <td>{{$order->user->surname.' '.$order->user->name.' '.$order->user->patronymic}}<br />
      Адрес: {{$order->user->address}}<br />
      Подъезд: {{$order->user->entrance}}<br />
      Квартира: {{$order->user->apartment}}<br />
      Этаж: {{$order->user->floor}}<br />
      Домофон: {{$order->user->intercom}}
      </td>
      <td>
      @foreach ($order->products as $product)
                    <p>
                        {{ $product->name. ' ' .$product->price. ' руб. ' .$product->pivot->qty. ' шт.' }}
                    </p>
                @endforeach
      </td>
      <td>
      {{$order->status}}
      @if ($order->status == 'новый')
          <a href="{{route('admin.orders.confirm', $order)}}" class="btn btn-burgundy">Подтвердить заказ</a>
      @endif
      </td>
      <td>
      @if ($order->status == 'отменен')
          Причина отмены: {{$order->comment}}
      @endif

      @if ($order->status == 'новый')
          <form method="post" action="{{route('admin.orders.cancel', $order)}}">
          @csrf
          <input type="text" name="comment" value="Комментарий">
          <button type="submit" class="btn btn-burgundy">Отменить заказ</button>
          </form>
      @endif
      </td>
    </tr>
    @endforeach
  </tbody>
    
</table>



@endsection