@extends('admin.layout')

@section('content')


<div class="container py-3 vh-100 element-animation">
        <h3 class="fw-light mb-5 text-center">Добавление нового товра</h3>
        <form class="row g-3" method="post" action="{{route('products.store')}}" enctype="multipart/form-data">
        @csrf
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputName" name="name" aria-describedy="name" placeholder="Название">
          </div>
          <div class="col-md-6">
            <input type="file" class="form-control" id="inputImg" name="img" aria-describedy="img" placeholder="Путь к картинке">
          </div>
          <div class="col-md-6">
            <input type="number" class="form-control" id="inputPrice" name="price" aria-describedy="price" placeholder="Цена">
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputSize" name="size" aria-describedy="size" placeholder="Размер">
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputDescription" name="description" aria-describedy="description" placeholder="Описание">
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputCompound" name="compound" aria-describedy="compound" placeholder="Состав">
          </div>
          <div class="col-md-6">
            <input type="number" class="form-control" id="inputQty" name="qty" aria-describedy="qty" placeholder="Количество">
          </div>
          <div class="col-md-6">
            <select class="form-select" aria-label="Категория товара" name="category_id">
            @foreach ($categories as $category)
                <option value="{{$category->id}}">{{$category->title}}</option>
            @endforeach
            </select>
          </div>
          <div class="col-12 text-center mt-5">
            <button type="submit" class="btn btn-burgundy">Создать</button>
          </div>
        </form>
      </div>

@endsection