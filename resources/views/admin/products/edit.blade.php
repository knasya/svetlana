@extends('admin.layout')

@section('content')


<div class="container py-3 vh-100 element-animation">
        <h3 class="fw-light mb-5 text-center">Изменение товра</h3>
        <form class="row g-3" method="post" action="{{route('products.update', $product)}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputName" name="name" aria-describedy="name" placeholder="Название" value="{{$product->name}}">
          </div>
          <div class="col-md-6">
            <input type="file" class="form-control" id="inputImg" name="img" aria-describedy="img" placeholder="Картинка" value="{{$product->img}}">
          </div>
          <div class="col-md-6">
            <input type="number" class="form-control" id="inputPrice" name="price" aria-describedy="price" placeholder="Цена" value="{{$product->price}}">
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputSize" name="size" aria-describedy="size" placeholder="Размер" value="{{$product->size}}">
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputDescription" name="description" aria-describedy="description" placeholder="Описание" value="{{$product->description}}">
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputCompound" name="compound" aria-describedy="compound" placeholder="Состав" value="{{$product->compound}}">
          </div>
          <div class="col-md-6">
            <input type="number" class="form-control" id="inputQty" name="qty" aria-describedy="qty" placeholder="Количество" value="{{$product->qty}}">
          </div>
          <div class="col-md-6">
            <select class="form-select" aria-label="Категория товара" name="category_id">
            @foreach ($categories as $category)
                <option value="{{$category->id}}" @if ($category->id == $product->category_id)
                    selected
                @endif>
                {{$category->title}}</option>
            @endforeach
            </select>
          </div>
          <div class="col-12 text-center mt-5">
            <button type="submit" class="btn btn-burgundy">Создать</button>
          </div>
        </form>
      </div>

@endsection