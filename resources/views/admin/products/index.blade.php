@extends('admin.layout')

@section('content')
    <!-- Каталог -->
    <div class="container py-3 element-animation">
        <div class="row">
            <div class="col-lg-12 text-center">

                <form action="{{ route('admin.products.filter') }}" method="post">
                    @csrf
                    <div class="d-flex justify-content-between mb-3">
                        <select class="form-select" name="filter">
                            <option value="0">Все категории</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                        <select name="sort" class="form-select">
                            <option value="name">По алфавиту</option>
                            <option value="price">Сначала дешевле</option>
                            <option value="created_at">Сначала новые</option>
                        </select>
                        <button class="btn btn-burgundy" type="submit">Применить</button>
                    </div>
                </form>

                <a class="btn btn-burgundy" href="{{ route('products.create') }}" role="button">Добавить новый товар</a>

                <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">


                    @foreach ($products as $product)
                        <div class="col card-hover">
                            <div class=" mb-4">
                                <div class="card-body">
                                    <div class="card-img-top card-border">
                                        <img src="/{{ $product->img }}" alt="Костюм" height="230">
                                    </div>
                                    <div class="card-body text-center">
                                        <h5 class="card-title">{{ $product->name }}</h5>
                                        <p class="card-text">{{ $product->price }} руб</p>
                                        <a class="btn btn-burgundy mb-3" href="{{ route('products.edit', $product) }}"
                                            role="button">Изменить</a>
                                        <form method="POST" action="{{ route('products.destroy', $product) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-burgundy" type="submit">Удалить</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>
        </div>
    </div>
@endsection
