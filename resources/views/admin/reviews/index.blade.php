@extends('admin.layout')

@section('content')

    <!-- Каталог -->
    <div class="container py-3 element-animation vh-100">
        <table class="table table-striped">
  <thead>
    <tr>
        <th scope="col">Имя</th>
        <th scope="col">Фамилия</th>
        <th scope="col">Отзыв</th>
        <th scope="col">Удалить</th>
        <th scope="col">Опубликовать</th>
        <th scope="col">Статус</th>
    </tr>
  </thead>
  <tbody>
    
    @foreach ($reviews as $review)
    <tr>
      <th scope="row">{{$review->name}}</th>
      <td>{{$review->surname}}</td>
      <td>{{$review->review}}</td>
      <td>
        <form method="POST" action="{{route('reviews.destroy', $review)}}">
            @csrf
            @method('DELETE')
            <button class="btn btn-burgundy" type="submit">Удалить</button>
        </form>
      </td>
      <td>
        <form method="POST" action="{{route('reviews.update', $review)}}">
            @csrf
            @method('PUT')
            <button class="btn btn-burgundy" type="submit" value="1">Опубликовать</button>
        </form>
      </td>
      <td>@if ($review->publish) <span>Опубликовано</span> @else <span>Не опубликовано</span> @endif</td>
    </tr>
    @endforeach
  </tbody>
    
</table>
    </div>

@endsection