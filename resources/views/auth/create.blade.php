@extends('layout')

@section('content')


    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="container py-3 vh-100 element-animation">
        <h3 class="fw-light mb-5 text-center">Регистрация</h3>
        <form class="row g-3" method="post" action="{{route('auth.store')}}">
        @csrf
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputName" name="name" placeholder="Имя" required>
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputSurname" name="surname" placeholder="Фамилия" required>
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputPatronymic" name="patronymic" placeholder="Отчество">
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputLogin" name="login" placeholder="Логин" required>
          </div>
          <div class="col-md-6">
            <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email" required>
          </div>
          <div class="col-md-6">
            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Пароль" required>
          </div>
          <div class="col-md-6">
            <input type="password" class="form-control" id="inputNumber" name="password_confirmation" placeholder="Повторите пароль" required>
          </div>
          {{-- <h5>Укажите адрес доставки</h5>
          <div class="col-md-12">
            <input type="text" class="form-control" id="inputAddress" name="address" placeholder="Улица, дом, город">
          </div>
          <div class="col-md-3">
            <input type="text" class="form-control" id="inputEntrance" name="entrance" placeholder="Подъезд">
          </div>
          <div class="col-md-3">
            <input type="text" class="form-control" id="inputApartment" name="apartment" placeholder="Квартира">
          </div>
          <div class="col-md-3">
            <input type="text" class="form-control" id="inputFloor" name="floor" placeholder="Этаж">
          </div>
          <div class="col-md-3">
            <input type="text" class="form-control" id="inputIntercom" name="intercom" placeholder="Домофон">
          </div> --}}
          <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" id="check" required>
            <label class="form-check-label" for="check">Согласен на обработку данных</label>
            </div>
          {!! RecaptchaV3::initJs() !!}
            {!! RecaptchaV3::field("validate_captcha") !!}
            <input type="hidden" name="action" value="validate_captcha">
            <div class="col-12 text-center mb-3">
                <button type="submit" class="btn btn-burgundy">Зарегистрироваться</button>
            </div>
        </form>
      </div>


@endsection