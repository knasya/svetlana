@extends('layout')

@section('content')

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="container py-3 vh-100 element-animation">
        <h3 class="fw-light mb-5 text-center">Вход</h3>
        <form class="row g-3" method="post" action="{{route('auth.login')}}">
        @csrf
          <div class="col-md-6">
            <input type="text" class="form-control" id="inputLogin" name="login" placeholder="Логин" value="{{old('login')}}" required>
          </div>
          <div class="col-md-6">
            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Пароль">
          </div>
          <div class="col-12 text-center mt-5">
            <button type="submit" class="btn btn-burgundy">Войти</button>
          </div>
        </form>
      </div>


      @endsection