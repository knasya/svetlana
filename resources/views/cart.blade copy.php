@extends('layout')

@section('content')

      <div class="container py-3 mb-3 element-animation vh-100">
        <h3 class="fw-light mb-5">Ваш заказ</h3>
        <table class="table table-striped mt-5">
          <tbody>
            @foreach ($products as $product)
                <tr>
                  <th scope="row">{{$product->name}}</th>
                  <th>{{$product->price}}</th>
                  <th>
                  <form method="post" action="{{route('cart.change', $product->id)}}">
                  @csrf
                  <input type="number" name="qty" value="{{$product->pivot->qty}}" min="0" max="{{$product->qty}}">
                  <button class="btn btn-burgundy" type="submit">Изменить</button>
                  <div class="from-text small">В наличии {{$product->qty}}</div>
                  </form>
                  </th>
                  <th>
                  <a class="btn btn-burgundy" href="{{route('cart.delete', $product->id)}}">Удалить</a>
                  </th>
                </tr>
            @endforeach
          </tbody>
        </table>

        <form method="PUT" action="{{route('cart')}}">
        @csrf
        @method('PUT')
        <h5>Укажите адрес доставки</h5>
          <div class="col-md-12">
            <input type="text" class="form-control" id="inputAddress" name="address" placeholder="Улица, дом, город" value="">
          </div>
          <div class="col-md-3">
            <input type="text" class="form-control" id="inputEntrance" name="entrance" placeholder="Подъезд" value="">
          </div>
          <div class="col-md-3">
            <input type="text" class="form-control" id="inputApartment" name="apartment" placeholder="Квартира" value="">
          </div>
          <div class="col-md-3">
            <input type="text" class="form-control" id="inputFloor" name="floor" placeholder="Этаж" value="">
          </div>
          <div class="col-md-3">
            <input type="text" class="form-control" id="inputIntercom" name="intercom" placeholder="Домофон" value="">
          </div>
          <button type="submit" class="btn btn-burgundy"  data-bs-toggle="modal" data-bs-target="#exampleModal">Оформить заказ</button>
          </form>

        <div class="alert alert-light mt-3 mb-3" role="alert">
        <b>Доставка осуществляется в течение 3-10 рабочих дней с момента заказа!</b>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Заголовок модального окна</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
      </div>
      <div class="modal-footer">
        <form method="POST" action="{{ route('orders.send') }}">
            @csrf
            <button type="submit" class="btn btn-burgundy">Оформить заказ</button>
        </form>
      </div>
    </div>
  </div>
</div>
      {{-- <form method="POST" action="{{ route('orders.send') }}">
            @csrf
            <button type="submit" class="btn btn-burgundy">Оформить заказ</button>
        </form> --}}
      </div>
      </div>
    </div>
  </div>


@endsection