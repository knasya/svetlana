@extends('layout')

@section('content')

      <div class="container py-3 mb-3 element-animation vh-100">
        <h3 class="fw-light mb-5">Ваш заказ</h3>
        <form method="post" action="{{ route('orders.send') }}">
        @csrf
        <table class="table table-striped mt-5">
          <tbody>
            @foreach ($products as $product)
                <tr>
                  <th scope="row">{{$product->name}}</th>
                  <th>{{$product->price}}</th>
                  <th>
                  <form method="post" action="{{route('cart.change', $product->id)}}">
                  @csrf
                  <input type="number" name="qty" value="{{$product->pivot->qty}}" min="0" max="{{$product->qty}}">
                  <button class="btn btn-burgundy" type="submit">Изменить</button>
                  <div class="from-text small">В наличии {{$product->qty}}</div>
                  </form>
                  </th>
                  <th>
                  <a class="btn btn-burgundy" href="{{route('cart.delete', $product->id)}}">Удалить</a>
                  </th>
                </tr>
            @endforeach
          </tbody>
        </table>
        <h5>Укажите адрес доставки</h5>
        <div class="col-md-12">
          <label for="address">Улица, дом, город</label>
          <input type="text" class="form-control" id="inputAddress" name="address" value="{{$user->address}}">
        </div>
        <div class="col-md-3">
          <label for="entrance">Подъезд</label>
          <input type="text" class="form-control" id="inputEntrance" name="entrance" value="{{$user->entrance}}">
        </div>
        <div class="col-md-3">
          <label for="apartment">Квартира</label>
          <input type="text" class="form-control" id="inputApartment" name="apartment" value="{{$user->apartment}}">
        </div>
        <div class="col-md-3">
          <label for="floor">Этаж</label>
          <input type="text" class="form-control" id="inputFloor" name="floor" value="{{$user->floor}}">
        </div>
        <div class="col-md-3">
          <label for="intercom">Домофон</label>
          <input type="text" class="form-control" id="inputIntercom" name="intercom" value="{{$user->intercom}}">
        </div>
            <button type="submit" class="btn btn-burgundy">Оформить заказ</button>
        </form>
      </div>
      </div>
    </div>
  </div>


@endsection