@extends('layout')

@section('content')

    <!-- Каталог -->
    <div class="container py-3 element-animation">
        <div class="row">
            <div class="col-lg-2 text-center">

            <form action="{{route('catalog.filter')}}" method="post">
                @csrf
                <div class="mb-3 mt-5">
                    <select class="form-select mb-3" name="filter">
                        <option value="0">Все категории</option>
                        @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>
                    <select name="sort" class="form-select mb-3">
                        <option value="name">По алфавиту</option>
                        <option value="price">Сначала дешевле</option>
                        <option value="created_at">Сначала новые</option>
                    </select>
                    <button class="btn btn-burgundy mb-3" type="submit">Применить</button>
                </div>
            </form>
            </div>

            <div class="col-lg-10">
            <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
           @foreach ($products as $product)
           <div class="col-sm-12 col-md-12 col-lg-4 card-hover">
           <a href = "{{route('product', ['id'=>$product->id])}}">
            <div class=" mb-4">
              <div class="card-body">
                <div class="card-img-top card-border">
                  <img src="/{{$product->img}}" class="" alt="Костюм" height="230">
                </div>
            <div class="card-body text-center">
              <h5 class="card-title">{{$product->name}}</h5>
              <p class="card-text mb-3">{{$product->price}} руб</p>
              @guest
                <a class="btn btn-burgundy" href="{{route('product', ['id'=>$product->id])}}" role="button">Подробнее</a>
              @endguest
              @auth
                <a class="btn btn-burgundy col-lg-5 me-3" href="{{route('cart.add', ['id'=>$product->id])}}" role="button">В КОРЗИНУ</a>
              @endauth
            </div>
              </div>
            </div>
            </a>
          </div>
        @endforeach
            </div>

           </div> 
           </div> 

@endsection