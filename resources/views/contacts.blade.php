@extends('layout')

@section('content')

      <div class="container py-3 text-center element-animation">
        <iframe class="mb-4" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.631143309294!2d37.73255351592832!3d55.695400380538025!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4fe9adf947d%3A0x4e41db0e695e3de2!2z0JvRjtCx0LvQuNC90YHQutCw0Y8g0YPQuy4sIDM5LCDQnNC-0YHQutCy0LAsIDEwOTM5MA!5e0!3m2!1sru!2sru!4v1670609273428!5m2!1sru!2sru" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        <div class="row">
          <div class="col-lg-4">
            <p class="mb-2 fw-light">Телефон</p>
            <p class="mb-2 fw-light">+7 (495) 823-54-12</p>
          </div>
          <div class="col-lg-4">
            <p class="mb-2 fw-light">Email</p>
            <p class="mb-2 fw-light">zolotova@gmail.com</p>
          </div>
          <div class="col-lg-4">
            <p class="mb-2 fw-light">Адрес</p>
            <p class="mb-2 fw-light">г. Москва, Люблинская 39/2</p>
          </div>
        </div>
      </div>

@endsection