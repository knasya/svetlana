@extends('layout')

@section('content')


      <!-- Баннер -->
      <div class="row banner">
        <div class="col-lg-6 text-center mb-3 d-flex align-items-center element-animation banner-text">
            <div class="col">
            <h2 class="fw-light mb-3">Студия Светланы Золотовой</h2>
            <h5 class="fw-light mb-3">Здесь вы можете приобрести лучшую <br /> одежду премиум класса</h5>
            <a class="btn btn-burgundy" href="{{route('catalog')}}" role="button">Смотреть каталог</a>
            </div>
        </div>
        <div class="col-lg-6 text-center">
            <img src="img/main.png" alt="banner" class="banner-img" height="560" width="370">
        </div>
      </div>

      <div class="cards mb-5 element-animation">
        <div class="container">
        <h2 class="fw-light mb-5 text-center">Наши преимущества</h2>
          <div class="cards-holder row">
            <div class="card col-md-12">
              <div class="text-center">
                <img src="img/ww.png" class="" alt="Костюм" height="90" width="90"> 
                <div class="card-body text-center">
                  <h5 class="card-title">Лучшая ткань</h5>
                  <p class="card-text">Мы используем только лучшие и натуральные ткани</p>
                </div>
              </div>
            </div>
            <div class="card col-md-12">
              <div class="text-center">
                <img src="img/qws.png" class="" alt="Костюм" height="90" width="90">
                <div class="card-body text-center">
                  <h5 class="card-title">Быстро</h5>
                  <p class="card-text">Доставка изделий в кратчайшие сроки</p>
                </div>
              </div>
            </div>
            <div class="card col-md-12">
              <div class="text-center">
                <img src="img/wm.png" class="" alt="Костюм" height="90" width="90">
                <div class="card-body text-center">
                  <h5 class="card-title">Качество</h5>
                  <p class="card-text">Все товары прослужат не менее 
                    5-ти лет</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <h2 class="fw-light mb-5 text-center element-animation">Новая коллекция</h2>
      <div class="container py-3 element-animation">
        <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
           @foreach ($products as $product)
           <div class="col-sm-12 col-md-12 col-lg-4 card-hover">
           <a href = "{{route('product', ['id'=>$product->id])}}">
            <div class=" mb-4">
              <div class="card-body">
                <div class="card-img-top card-border">
                  <img src="/{{$product->img}}" class="" alt="Костюм" height="230">
                </div>
            <div class="card-body text-center">
              <h5 class="card-title">{{$product->name}}</h5>
              <p class="card-text mb-3">{{$product->price}} руб</p>
              @guest
                <a class="btn btn-burgundy" href="{{route('product', ['id'=>$product->id])}}" role="button">Подробнее</a>
              @endguest
              @auth
                <a class="btn btn-burgundy col-lg-5 me-3" href="{{route('cart.add', ['id'=>$product->id])}}" role="button">В КОРЗИНУ</a>
              @endauth
            </div>
              </div>
            </div>
            </a>
          </div>
        @endforeach
            </div>
          </div>

        </div>
      </div>

      <!-- О бренде -->
      <div class="container py-3 element-animation about">
        <h2 class="fw-light mb-5 text-center element-animation">О бренде</h2>
      <div class="row mb-5">
        <div class="col-lg-6 ext-center">
            <img class="mx-auto d-block" src="img/11.png" alt="banner" height="400" width="430">
        </div>
        <div class="col-lg-6 text-center mb-3 d-flex align-items-center">
            <div class="col">
            <h4 class="fw-light mb-3">Одежда премиум класса достойная Вас! От дизайнера <br /> Светланы Золотовой. Комфорт и качество, <br /> быстрая доставка и примерка.</h4>
            </div>
        </div>
      </div>
      <div class="row mb-5">
        <div class="col-lg-6 text-center mb-3 d-flex align-items-center about-text">
            <div class="col">
            <h4 class="fw-light mb-3">Возможность заказать модель одежды с учетом <br /> ваших мерок. Помощь стилиста. Производство <br /> находится в Москве.</h4>
            </div>
        </div>
        <div class="col-lg-6 ext-center">
            <img class="mx-auto d-block img-top" src="img/10.png" alt="banner" height="400" width="450">
        </div>
      </div>
      </div>


@endsection