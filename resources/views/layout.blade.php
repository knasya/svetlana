<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <title>Студия Светланы Золотовой</title>
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-white element-animation">
        <div class="container-fluid">
          <a class="navbar-brand" href="{{route('index')}}"><img src="/img/logo.png" alt="logo" height="50" width="50"></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link" href="{{route('catalog')}}">Каталог</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('aboutBrand')}}">О бренде</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('order')}}">На заказ</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('contacts')}}">Контакты</a>
              </li>
            </ul>
            <form class="d-flex align-items-center">
                <div class="text-reset me-3">
                @guest
                    <a class="btn btn-burgundy" href="{{route('auth.create')}}">Зарегистрироваться</a>
                    <a class="btn btn-burgundy" href="{{route('auth.loginform')}}">Войти</a>
                @endguest
                </div>
                <div class="text-reset me-3">
                @auth
                    <a class="btn btn-burgundy" href="{{route('auth.logout')}}">Выйти</a>
                @endauth
                </div>
                <div class="text-reset me-3">
                @auth
                    <a href="{{route('cart.show')}}"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-cart2" viewBox="0 0 16 16">
                        <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l1.25 5h8.22l1.25-5H3.14zM5 13a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
                      </svg></a>
                      <a href="{{route('orders.list')}}">
                      <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
  <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
  <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
</svg>
                      </a>
                @endauth
                </div>
            </form>
          </div>
        </div>
      </nav>
      
      <div>
        @if (session('info'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session('info')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Закрыть">
                </button>
            </div>
        @endif
        @yield('content')
      </div>

     <div class="footer pt-5 element-animation">
          <div class="col d-flex justify-content-evenly mb-5">
            <a class="fw-light" href="{{route('catalog')}}">Каталог</a>
            <a class="fw-light" href="{{route('aboutBrand')}}">О бренде</a>
            <a class="fw-light" href="{{route('order')}}">На заказ</a>
            <a class="fw-light" href="{{route('contacts')}}">Контакты</a>
          </div>
          <p class="text-center fw-light pb-5">
            © 2023 Все права защищены «Студия Светланы Золотовой»
          </p>
      </div>

    <script src="/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>