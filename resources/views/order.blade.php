@extends('layout')

@section('content')



    <div class="row banner pt-5 pb-5 element-animation mb-5">
        <div class="col-lg-6 text-center mb-3 d-flex align-items-center">
            <div class="col">
                <h2 class="fw-light mb-3">Пошив одежды на заказ</h2>
                <h5 class="fw-light mb-3">Студия Светланы Золотовой занимаемся пошивом одежды на заказ. <br /> Вы можете
                    оставть заявку или позвонить по номеру <br /> <b>+7 (916) 151-41-78</b></h5>
                <a class="btn btn-burgundy" href="#form" role="button">Оставить заявку</a>
            </div>
        </div>
        <div class="col-lg-6 text-center banner-img">
            <div class="container py-3">
                <!-- <h2 class="fw-light mb-5 text-center">Наши работы</h2> -->
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0"
                            class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                            aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                            aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active text-center">
                            <img src="img/4.png" class="img-border" alt="..." height="500">
                        </div>
                        <div class="carousel-item text-center">
                            <img src="img/3.png" class="img-border" alt="..." height="500">
                        </div>
                        <div class="carousel-item text-center">
                            <img src="img/5.png" class="img-border" alt="..." height="500">
                        </div>
                    </div>
                    </button>
                </div>
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Форма -->
    <div class="container py-3 element-animation">
        <h2 class="fw-light mb-5 text-center">Оставить заявку</h2>
        <form class="row g-3" id="response-form" method="post" action="{{ route('order') }}">
            @csrf
            <div class="col-md-6">
                <label for="inputName" class="form-label" for="name">Имя</label>
                <input type="text" class="form-control" id="inputName" name="name">
            </div>
            <div class="col-md-6">
                <label for="inputNumber" class="form-label" for="phone">Телефон</label>
                <input type="text" maxlength="11" name="phone" value="7" class="form-control" id="inputNumber">
            </div>
            <div class="col-md-12">
                <label for="inputApplication" class="form-label" for="application">Комментарий</label>
                <input type="text" name="application" class="form-control" id="inputApplication">
            </div>
            {!! RecaptchaV3::initJs() !!}
            {!! RecaptchaV3::field("validate_captcha") !!}
            <input type="hidden" name="action" value="validate_captcha">
            <div class="col-12 text-center mb-3">
                <button type="submit" class="btn btn-burgundy">Отправить</button>
            </div>
        </form>
    </div>

@endsection
