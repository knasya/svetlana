@extends('layout')

@section('content')

<div class="container vh-100">
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Дата</th>
        <th scope="col">Статус</th>
        <th scope="col">Состав заказа</th>
    </tr>
</thead>
<tbody>
    @foreach ($orders as $order)
        <tr>
            <th scope="row">{{ $order->created_at }}</th>
            <td>{{ $order->status }}
                @if($order->status == 'новый')
                    <a href="{{ route('orders.destroy', $order) }}" class="btn btn-secondary">Удалить</a>
                @endif
            </td>

            <td>
                @foreach ($order->products as $product)
                    <p>
                        {{ $product->name. ' ' .$product->price. ' руб. ' .$product->pivot->qty. ' шт.' }}
                    </p>
                @endforeach
            </td>

        </tr>
    @endforeach
</tbody>
</table>
</div>

      

@endsection