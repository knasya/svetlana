@extends('layout')

@section('content')

      <!-- Карточка -->
      <div class="container py-3 element-animation">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{route('index')}}">Главная</a></li>
              <li class="breadcrumb-item"><a href="{{route('catalog')}}">Каталог</a></li>
              <li class="breadcrumb-item active" aria-current="page">{{$product->name}}</li>
            </ol>
          </nav>
          <div class="row">
            <div class="col-lg-6 row mb-5">
                <div class="col-lg-9">
                    <img src="/{{$product->img}}" alt="" height="450">
                </div>
            </div>
            <div class="col-lg-6">
                <h4 class="mb-3 fw-light">{{$product->name}}</h4>
                <p class="mb-3 fw-bolder">{{$product->price}} рублей</p>
                <h5 class="mb-3 fw-light">Размер:</h5>
                <h5 class="mb-3 fw-light">{{$product->size}}</h5>
                <div class="row mb-3">
                @auth
                    <a class="btn btn-burgundy col-lg-5 me-3" href="{{route('cart.add', ['id'=>$product->id])}}" role="button">В КОРЗИНУ</a>
                @endauth
                    <a class="btn btn-burgundy col-lg-5" href="{{route('auth.create')}}" role="button">ЗАРЕГИСТРИРОВАТЬСЯ И КУПИТЬ</a>
                </div>
                <h6 class="mb-3 fw-light">Подробности</h6>
                <div class="accordion mb-3" id="accordionPanelsStayOpenExample">
                    <div class="accordion-item">
                      <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                          Обмеры и описание
                        </button>
                      </h2>
                      <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                        <div class="accordion-body">
                          {{$product->description}}
                        </div>
                      </div>
                    </div>
                    <div class="accordion-item">
                      <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
                          Состав и уход
                        </button>
                      </h2>
                      <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingTwo">
                        <div class="accordion-body">
                            {{$product->compound}}
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
          </div>




@endsection