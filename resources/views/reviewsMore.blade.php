@extends('layout')

@section('content')


      <!-- Отзывы -->
      <h2 class="fw-light mb-5 text-center element-animation">Отзывы</h2>

      <div class="row element-animation mb-5">
      @foreach ($reviews as $review)
          <div class="cards-holder col-lg-4 mb-3">
          <div class="card" style="margin-top: 0;">
            <div class="text-center">
              <div class="card-body text-center">
                <h5 class="card-title">{{$review->name}}</h5>
                <p class="card-text">{{$review->review}}</p>
              </div>
            </div>
          </div>
        </div>
      @endforeach
      </div>


@endsection