<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;

//Фасады
use Illuminate\Support\Facades\Auth;


//Контроллеры
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\CartController;
//Модели
use App\Models\Product;
use App\Models\Category;
use App\Models\Review;
use App\Models\User;
use App\Models\Order;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index', ['products'=>Product::orderBy('created_at', 'desc')->limit(3)->where('qty', '>', '0')->get()]);
})->name('index');

Route::get('/catalog', function () {
    $products = Product::where('qty', '>', '0')->get();
    return view('catalog', ['products'=>$products, 'categories'=>Category::all()]);
})->name('catalog');

Route::get('/product/{id}', function ($id) {
    return view('product', ['product'=>Product::find($id)]);
})->name('product');

Route::post('/catalog/filter', function (Request $request){
    if ($request->filter == 0){
        $products = Product::where('qty', '>', '0')
                            ->orderBy($request->sort)
                            ->get();
    } else {
        $products = Category::find($request->filter)
                            ->products()
                            ->orderBy($request->sort)
                            ->get();
    }
    return view('catalog', ['products'=>$products, 'categories'=>Category::all()]);
})->name('catalog.filter');

Route::get('/aboutBrand', function () {
    return view('aboutBrand');
})->name('aboutBrand');

Route::get('/basket', function () {
    return view('basket');
})->name('basket');

Route::get('/contacts', function () {
    return view('contacts');
})->name('contacts');

Route::get('/dress', function () {
    return view('dress');
})->name('dress');

Route::get('/favorites', function () {
    return view('favorites');
})->name('favorites');

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::get('/myOrders', function () {
    return view('myOrders');
})->name('myOrders');

Route::get('/order', function () {
    return view('order');
})->name('order');

Route::get('/profile', function () {
    return view('profile');
})->name('profile');

Route::get('/registration', function () {
    return view('registration');
})->name('registration');


Route::middleware(['guest'])->group(function() {
    Route::get('/register', [RegisterController::class, 'create'])->name('auth.create');
    Route::post('/register', [RegisterController::class, 'store'])->name('auth.store');

    Route::get('/login', [RegisterController::class, 'loginform'])->name('auth.loginform');
    Route::post('/login', [RegisterController::class, 'login'])->name('auth.login');
});

Route::middleware(['auth'])->group(function() {
    Route::get('/logout', [RegisterController::class, 'logout'])->name('auth.logout');
    Route::get('/cart', [RegisterController::class, 'update'])->name('cart');
    Route::get('cart/add/{id}', [CartController::class, 'add'])->name('cart.add');
    Route::get('cart/show', [CartController::class, 'show'])->name('cart.show');
    Route::post('cart/change/{id}', [CartController::class, 'change'])->name('cart.change');
    Route::get('cart/delete/{id}', [CartController::class, 'delete'])->name('cart.delete');
    Route::post('orders/send', [OrderController::class, 'send'])->name('orders.send');
    Route::get('orders/list', [OrderController::class, 'list'])->name('orders.list');
    Route::get('orders/destroy/{order}', [OrderController::class, 'destroy'])->name('orders.destroy');
});

Route::middleware(['admin'])->group(function() {
    Route::view('/admin', 'admin.layout');
    Route::resource('/admin/categories', CategoryController::class);
    Route::resource('/admin/products', ProductController::class);
    Route::post('/admin/filter', [ProductController::class, 'filter'])->name('admin.products.filter');
    Route::get('/admin/orders', [OrderController::class, 'index'])->name('admin.orders.index');
    Route::get('/admin/{order}/confirm/', [OrderController::class, 'confirm'])->name('admin.orders.confirm');
    Route::post('/admin/{order}/cancel', [OrderController::class, 'cancel'])->name('admin.orders.cancel');
    Route::resource('/admin/applications', ApplicationController::class);
    Route::resource('/admin/reviews', ReviewController::class);
});

Route::post('/order', [ApplicationController::class, 'application'])->name('application');
Route::post('/aboutBrand', [ReviewController::class, 'review'])->name('review');
Route::get('/reviewsMore', function () {
    $reviews = Review::orderBy('updated_at')->where('publish', '=', '1')->get();
    return view('reviewsMore', ['reviews'=>$reviews]);
})->name('reviewsMore');